<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BienApiTest extends TestCase
{
    use MakeBienTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBien()
    {
        $bien = $this->fakeBienData();
        $this->json('POST', '/api/v1/biens', $bien);

        $this->assertApiResponse($bien);
    }

    /**
     * @test
     */
    public function testReadBien()
    {
        $bien = $this->makeBien();
        $this->json('GET', '/api/v1/biens/'.$bien->id);

        $this->assertApiResponse($bien->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBien()
    {
        $bien = $this->makeBien();
        $editedBien = $this->fakeBienData();

        $this->json('PUT', '/api/v1/biens/'.$bien->id, $editedBien);

        $this->assertApiResponse($editedBien);
    }

    /**
     * @test
     */
    public function testDeleteBien()
    {
        $bien = $this->makeBien();
        $this->json('DELETE', '/api/v1/biens/'.$bien->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/biens/'.$bien->id);

        $this->assertResponseStatus(404);
    }
}
