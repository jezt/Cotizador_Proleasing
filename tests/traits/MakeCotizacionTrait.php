<?php

use Faker\Factory as Faker;
use App\Models\Cotizacion;
use App\Repositories\CotizacionRepository;

trait MakeCotizacionTrait
{
    /**
     * Create fake instance of Cotizacion and save it in database
     *
     * @param array $cotizacionFields
     * @return Cotizacion
     */
    public function makeCotizacion($cotizacionFields = [])
    {
        /** @var CotizacionRepository $cotizacionRepo */
        $cotizacionRepo = App::make(CotizacionRepository::class);
        $theme = $this->fakeCotizacionData($cotizacionFields);
        return $cotizacionRepo->create($theme);
    }

    /**
     * Get fake instance of Cotizacion
     *
     * @param array $cotizacionFields
     * @return Cotizacion
     */
    public function fakeCotizacion($cotizacionFields = [])
    {
        return new Cotizacion($this->fakeCotizacionData($cotizacionFields));
    }

    /**
     * Get fake data of Cotizacion
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCotizacionData($cotizacionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'uri_pdf' => $fake->word,
            'nombre_cliente' => $fake->word,
            'apellido_paterno_cliente' => $fake->word,
            'apellido_materno_cliente' => $fake->word,
            'empresa_cliente' => $fake->word,
            'telefono_cliente' => $fake->randomDigitNotNull,
            'email_cliente' => $fake->word,
            'bien_id' => $fake->randomDigitNotNull,
            'valor_factura_sin_iva' => $fake->word,
            'porcentaje_comision' => $fake->word,
            'plazo' => $fake->randomDigitNotNull,
            'porcentaje_pago_inicial' => $fake->word,
            'valor_residual_calculo' => $fake->word,
            'porcentaje_adicional_vr_impreso' => $fake->word,
            'porcentaje_tasa' => $fake->word,
            'pago_inicial' => $fake->word,
            'subtotal' => $fake->word,
            'pago_mensual_fijo' => $fake->word,
            'iva' => $fake->word,
            'renta_en_deposito' => $fake->word,
            'total_pago_inicial' => $fake->word,
            'valor_residual_sin_iva' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $cotizacionFields);
    }
}
