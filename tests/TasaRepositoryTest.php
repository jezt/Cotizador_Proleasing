<?php

use App\Models\Tasa;
use App\Repositories\TasaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TasaRepositoryTest extends TestCase
{
    use MakeTasaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TasaRepository
     */
    protected $tasaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->tasaRepo = App::make(TasaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTasa()
    {
        $tasa = $this->fakeTasaData();
        $createdTasa = $this->tasaRepo->create($tasa);
        $createdTasa = $createdTasa->toArray();
        $this->assertArrayHasKey('id', $createdTasa);
        $this->assertNotNull($createdTasa['id'], 'Created Tasa must have id specified');
        $this->assertNotNull(Tasa::find($createdTasa['id']), 'Tasa with given id must be in DB');
        $this->assertModelData($tasa, $createdTasa);
    }

    /**
     * @test read
     */
    public function testReadTasa()
    {
        $tasa = $this->makeTasa();
        $dbTasa = $this->tasaRepo->find($tasa->id);
        $dbTasa = $dbTasa->toArray();
        $this->assertModelData($tasa->toArray(), $dbTasa);
    }

    /**
     * @test update
     */
    public function testUpdateTasa()
    {
        $tasa = $this->makeTasa();
        $fakeTasa = $this->fakeTasaData();
        $updatedTasa = $this->tasaRepo->update($fakeTasa, $tasa->id);
        $this->assertModelData($fakeTasa, $updatedTasa->toArray());
        $dbTasa = $this->tasaRepo->find($tasa->id);
        $this->assertModelData($fakeTasa, $dbTasa->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTasa()
    {
        $tasa = $this->makeTasa();
        $resp = $this->tasaRepo->delete($tasa->id);
        $this->assertTrue($resp);
        $this->assertNull(Tasa::find($tasa->id), 'Tasa should not exist in DB');
    }
}
