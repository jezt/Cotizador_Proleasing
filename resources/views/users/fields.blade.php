<div class="form-group col-sm-12">
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Datos del usuario</span>
        <h3 class="page-title">Llene el formulario con los datos solicitados</h3>
        </div>
    </div>
</div>
<div class="form-group col-sm-12 row">
    <!-- Name Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('name', 'Nombre:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Apellido Paterno Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('apellido_paterno', 'Apellido Paterno:') !!}
        {!! Form::text('apellido_paterno', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Apellido Materno Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('apellido_materno', 'Apellido Materno:') !!}
        {!! Form::text('apellido_materno', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'Correo electrónico:') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group col-sm-12 row">
    <!-- Password Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('password', 'Contraseña:') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <!-- Tipo User Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('tipo_user', 'Tipo usuario:') !!}
        <select class="form-control" id="selectTipoUser" name="tipoUser">
            <option value="" disabled selected>Selecciona el tipo de usuario</option>
            <option value='1'>Administrador</option>
            <option value='2'>Empleado</option>
        </select>   
    </div>
</div>

<!-- Remember Token Field -->
<!-- <div class="form-group col-sm-6">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12" style="text-align:center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-secondary">Cancelar</a>
</div>
