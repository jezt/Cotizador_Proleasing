@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>
            <!-- Valor Tasa -->
        </h1>
    </section>
    <div class="content" style="width:100%">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    <div class="main-content-container container-fluid px-4">
                        {!! Form::open(['route' => 'valorTasas.store']) !!}

                            @include('valor_tasas.fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#bien-table').DataTable({
            responsive: true,
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json' //Ubicacion del archivo con el json del idioma.
            }
        
        });
        $('#plazoResidual').DataTable({
            responsive: true,
            language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json' //Ubicacion del archivo con el json del idioma.
            }
        
        });
    });

    function tipoBien(dato)
        {
            if(dato == 5)
            {
                $('#vr_campo').show();
            }
            else{
                $('#vr_campo').hide();                
            }
        }
</script>
@endsection