<table class="table table-responsive-sm" id="valorTasas-table">
    <thead>
        <tr>
        <th>Tasa</th>
        <th>Valor</th>
        <th>Disponible para</th>
        <th>Aplica en</th>
        <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach($valorTasas as $valorTasa)
    <?php
    $array_bien = explode(",", $valorTasa->bien_section);    
    ?>
        <tr>
            <td>{!! $valorTasa->tasa->nombre !!}</td>
            <td>
                @if($valorTasa->tasa_id == 1 || $valorTasa->tasa_id == 3 || $valorTasa->tasa_id == 4 || $valorTasa->tasa_id == 5 || $valorTasa->tasa_id == 6)
                    {!! $valorTasa->valor !!}% 
                @elseif($valorTasa->tasa_id == 2)
                    {!! $valorTasa->valor !!} meses
                @elseif($valorTasa->tasa_id == 7)
                    ${!! $valorTasa->valor !!}
                @endif
            </td>
            <td>
            @foreach($array_bien as $bien)
            <ul>
                <li>{!! $bien !!}</li>
            </ul>
            @endforeach
            </td>
            <td>
            @if($valorTasa->plazo_vr != null)
            {!! $valorTasa->plazo_vr !!} meses
            @else
            Todos los meses
            @endif
            </td>
            <td>
                {!! Form::open(['route' => ['valorTasas.destroy', $valorTasa->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('valorTasas.edit', [$valorTasa->id]) !!}" class='btn btn-primary btn-xs'><i class="far fa-edit"></i></a>
                    <!-- {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Esta seguro de eliminar el valor de esta tasa?')"]) !!} -->
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>