<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTasaRequest;
use App\Http\Requests\UpdateTasaRequest;
use App\Repositories\TasaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TasaController extends AppBaseController
{
    /** @var  TasaRepository */
    private $tasaRepository;

    public function __construct(TasaRepository $tasaRepo)
    {
        $this->tasaRepository = $tasaRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Tasa.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tasaRepository->pushCriteria(new RequestCriteria($request));
        $tasas = $this->tasaRepository->all();

        return view('tasas.index')
            ->with('tasas', $tasas);
    }

    /**
     * Show the form for creating a new Tasa.
     *
     * @return Response
     */
    public function create()
    {
        return view('tasas.create');
    }

    /**
     * Store a newly created Tasa in storage.
     *
     * @param CreateTasaRequest $request
     *
     * @return Response
     */
    public function store(CreateTasaRequest $request)
    {
        $input = $request->all();

        $tasa = $this->tasaRepository->create($input);

        Flash::success('Tasa creada correctamente.');

        return redirect(route('tasas.index'));
    }

    /**
     * Display the specified Tasa.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tasa = $this->tasaRepository->findWithoutFail($id);

        if (empty($tasa)) {
            Flash::error('Tasa not found');

            return redirect(route('tasas.index'));
        }

        return view('tasas.show')->with('tasa', $tasa);
    }

    /**
     * Show the form for editing the specified Tasa.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tasa = $this->tasaRepository->findWithoutFail($id);

        if (empty($tasa)) {
            Flash::error('Tasa not found');

            return redirect(route('tasas.index'));
        }

        return view('tasas.edit')->with('tasa', $tasa);
    }

    /**
     * Update the specified Tasa in storage.
     *
     * @param  int              $id
     * @param UpdateTasaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTasaRequest $request)
    {
        $tasa = $this->tasaRepository->findWithoutFail($id);

        if (empty($tasa)) {
            Flash::error('Tasa not found');

            return redirect(route('tasas.index'));
        }

        $tasa = $this->tasaRepository->update($request->all(), $id);

        Flash::success('Tasa actualizada correctamente.');

        return redirect(route('tasas.index'));
    }

    /**
     * Remove the specified Tasa from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tasa = $this->tasaRepository->findWithoutFail($id);

        if (empty($tasa)) {
            Flash::error('Tasa not found');

            return redirect(route('tasas.index'));
        }

        $this->tasaRepository->delete($id);

        Flash::success('Tasa eliminada correctamente.');

        return redirect(route('tasas.index'));
    }

    public function getValores($id)
    {
        $valores = \App\Models\valorTasa::where('tasa_id','=',$id)->get();
        
        return view('tasas.valores')->with('valores',$valores);
    }
}
