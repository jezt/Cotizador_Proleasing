<?php

namespace App\Repositories;

use App\Models\Cotizacion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CotizacionRepository
 * @package App\Repositories
 * @version November 9, 2018, 6:07 pm UTC
 *
 * @method Cotizacion findWithoutFail($id, $columns = ['*'])
 * @method Cotizacion find($id, $columns = ['*'])
 * @method Cotizacion first($columns = ['*'])
*/
class CotizacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'uri_pdf',
        'nombre_cliente',
        'apellido_paterno_cliente',
        'apellido_materno_cliente',
        'empresa_cliente',
        'telefono_cliente',
        'email_cliente',
        'bien_id',
        'valor_factura_sin_iva',
        'porcentaje_comision',
        'plazo',
        'porcentaje_pago_inicial',
        'valor_residual_calculo',
        'porcentaje_adicional_vr_impreso',
        'porcentaje_tasa',
        'pago_inicial',
        'subtotal',
        'pago_mensual_fijo',
        'iva',
        'renta_en_deposito',
        'total_pago_inicial',
        'valor_residual_sin_iva'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Cotizacion::class;
    }
}
