<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Tasa
 * @package App\Models
 * @version November 9, 2018, 6:06 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection cotizaciones
 * @property \Illuminate\Database\Eloquent\Collection ValoresTasa
 * @property string nombre
 */
class Tasa extends Model
{
    use SoftDeletes;

    public $table = 'tasas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function valoresTasas()
    {
        return $this->hasMany(\App\Models\ValoresTasa::class);
    }
}
